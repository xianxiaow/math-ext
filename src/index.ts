const getFloatLen = (n) => {
  let ret = 0;
  try {
    ret = n.toString().split(".")[1].length
  } catch(e){
    ret = 0
  };
  return ret;
}

/**
 * 加
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export const add = (arg1: number, arg2: number) => {
  const r1 = getFloatLen(arg1);
  const r2 = getFloatLen(arg2);
  const n = Math.max(r1, r2)
  const m = Math.pow(10, n);
  const v1 = mul(arg1, m);
  const v2 = mul(arg2, m);
  return (v1 + v2) / m;
}

/**
 * 减
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export const sub = (arg1: number, arg2: number) => {
  const r1 = getFloatLen(arg1);
  const r2 = getFloatLen(arg2);
  const n = Math.max(r1, r2);
  const m = Math.pow(10, n);
  const v1 = mul(arg1, m);
  const v2 = mul(arg2, m);
  const value = (v1 - v2) / m;
  return Number(value.toFixed(n));
}

/**
 * 乘
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export const mul = (arg1: number, arg2: number) => {
  const r1 = getFloatLen(arg1);
  const r2 = getFloatLen(arg2);
  const s1 = Number(arg1.toString().replace('.', ''));
  const s2 = Number(arg2.toString().replace('.', ''));
  return s1 * s2 / Math.pow(10, r1 + r2);
}

/**
 * 除
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export const div = (arg1: number, arg2: number) => {
  const r1 = getFloatLen(arg1);
  const r2 = getFloatLen(arg2);
  const s1 = Number(arg1.toString().replace('.', ''));
  const s2 = Number(arg2.toString().replace('.', ''));
  return (s1 / s2) * Math.pow(10, r2 - r1);
}
