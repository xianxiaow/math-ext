/**
 * 加
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export declare const add: (arg1: number, arg2: number) => number;
/**
 * 减
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export declare const sub: (arg1: number, arg2: number) => number;
/**
 * 乘
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export declare const mul: (arg1: number, arg2: number) => number;
/**
 * 除
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export declare const div: (arg1: number, arg2: number) => number;
