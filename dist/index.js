var getFloatLen = function (n) {
    var ret = 0;
    try {
        ret = n.toString().split(".")[1].length;
    }
    catch (e) {
        ret = 0;
    }
    ;
    return ret;
};
/**
 * 加
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export var add = function (arg1, arg2) {
    var r1 = getFloatLen(arg1);
    var r2 = getFloatLen(arg2);
    var n = Math.max(r1, r2);
    var m = Math.pow(10, n);
    var v1 = mul(arg1, m);
    var v2 = mul(arg2, m);
    return (v1 + v2) / m;
};
/**
 * 减
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export var sub = function (arg1, arg2) {
    var r1 = getFloatLen(arg1);
    var r2 = getFloatLen(arg2);
    var n = Math.max(r1, r2);
    var m = Math.pow(10, n);
    var v1 = mul(arg1, m);
    var v2 = mul(arg2, m);
    var value = (v1 - v2) / m;
    return Number(value.toFixed(n));
};
/**
 * 乘
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export var mul = function (arg1, arg2) {
    var r1 = getFloatLen(arg1);
    var r2 = getFloatLen(arg2);
    var s1 = Number(arg1.toString().replace('.', ''));
    var s2 = Number(arg2.toString().replace('.', ''));
    return s1 * s2 / Math.pow(10, r1 + r2);
};
/**
 * 除
 * @param arg1 number
 * @param arg2 number
 * @return number
 */
export var div = function (arg1, arg2) {
    var r1 = getFloatLen(arg1);
    var r2 = getFloatLen(arg2);
    var s1 = Number(arg1.toString().replace('.', ''));
    var s2 = Number(arg2.toString().replace('.', ''));
    return (s1 / s2) * Math.pow(10, r2 - r1);
};
